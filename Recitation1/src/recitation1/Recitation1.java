/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author NanJiang219
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        Button gb = new Button();
        btn.setText("Say 'Hello World'");
        gb.setText("Say 'Goodbye Cruel World!'");//New button
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        //New button's action
        gb.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Goodbye Cruel World!");
            }
        });
        //Change the pane type to make them show up together
        FlowPane root = new FlowPane();
        root.getChildren().add(btn);
        root.getChildren().add(gb);//add the new button to the pane
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
